const functions = require("firebase-functions");

require('dotenv').config();

const stripe = require('stripe')(process.env.STRIPE_KEY)

exports.stripeCreatePaymentIntent = functions.https.onCall((data, context) => {
    return new Promise((resolve, reject) => {
        stripe.paymentIntents.create({
            amount: data.amount,
            currency: data.currency
        },
            function (err, paymentIntent) {
                if (err != null) {
                    console.log('err', err)
                    reject(err)
                }
                else {
                    resolve({ client_secret: paymentIntent.client_secret })
                }
            })
    })

})

exports.stripeCreateCheckoutSession = functions.https.onCall((data, context) => {
    try {
        const session = stripe.checkout.sessions.create({
            payment_method_types: ['card'],
            success_url: 'https://checkout.stripe.dev/success',
            cancel_url: 'https://checkout.stripe.dev/cancel',
            line_items: data['line_items'],
            mode: 'payment',
        });
        return session
    } catch (error) {
        return error
    }
})