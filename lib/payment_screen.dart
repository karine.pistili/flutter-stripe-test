import 'package:flutter/material.dart';
import 'dart:io';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:flutter_stripe/flutter_stripe.dart';
import 'package:flutter_stripe_test/service/payment_service.dart';

class PaymentScreen extends StatefulWidget {
  const PaymentScreen({Key? key}) : super(key: key);

  @override
  State<PaymentScreen> createState() => _PaymentScreenState();
}

class _PaymentScreenState extends State<PaymentScreen> {
  final controller = CardEditController();

  Future<void> _handleCreditCardPayment() async {
    if (!controller.details.complete) {
      return;
    }
    const billingDetails = BillingDetails(
      email: 'karine.pistili@gmail.com',
      phone: '+48888000888',
      address: Address(
        city: 'Houston',
        country: 'US',
        line1: '1459  Circle Drive',
        line2: '',
        state: 'Texas',
        postalCode: '77063',
      ),
    ); // mocked data for tests

    int amount = 100;
    String currency = 'eur';

    try {
      PaymentResponseHandler res = await PaymentService.handleCreditCardPayment(
          billingDetails, amount, currency);
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(res.message),
        ),
      );
    } catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            e.toString(),
          ),
        ),
      );
    }
  }

  Future<void> _handleCheckoutPayment() async {
    List products = [
      {
        'price_data': {
          'currency': 'brl',
          'product_data': {
            'name': 'Stubborn Attachments',
            'images': ['https://i.imgur.com/EHyR2nP.png'],
          },
          'unit_amount': 2000,
        },
        'quantity': 2
      },
    ];

    try {
      final res = await PaymentService.handleCheckoutPayment(
          context, {'line_items': products});

      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(res.message),
        ),
      );

      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => MyCheckoutScreen(
                  url: res.url,
                )),
      );
    } catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            e.toString(),
          ),
        ),
      );
    }
  }

  @override
  void initState() {
    super.initState();
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Padding(
        padding: const EdgeInsets.all(18.0),
        child: Column(
          children: [
            const Text(
              'PAY WITH CREDIT CARD',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            CardField(
              controller: controller,
            ),
            ElevatedButton(
              onPressed: () async {
                await _handleCreditCardPayment();
              },
              child: const Text('PAY'),
            ),
            const Padding(
              padding: EdgeInsets.symmetric(vertical: 18.0),
              child: Divider(thickness: 1),
            ),
            const Text(
              'PAY WITH CHECKOUT METHOD',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            Center(
              child: ElevatedButton(
                onPressed: () async {
                  await _handleCheckoutPayment();
                },
                child: const Text('CHECKOUT'),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class MyCheckoutScreen extends StatefulWidget {
  final String url;
  const MyCheckoutScreen({Key? key, required this.url}) : super(key: key);

  @override
  State<MyCheckoutScreen> createState() => _MyCheckoutScreenState();
}

class _MyCheckoutScreenState extends State<MyCheckoutScreen> {
  @override
  Widget build(BuildContext context) {
    return WebView(
      initialUrl: widget.url,
      javascriptMode: JavascriptMode.unrestricted,
    );
  }
}
