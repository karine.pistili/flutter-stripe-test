import 'package:cloud_functions/cloud_functions.dart';

class CloudFunctionsService {
 
  static Future callFunction(String functionName, payload) async {
    HttpsCallable callable =
        FirebaseFunctions.instance.httpsCallable(functionName);
    return await callable.call(payload);
  }

}