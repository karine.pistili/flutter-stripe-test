import 'package:flutter_stripe/flutter_stripe.dart';
import 'package:cloud_functions/cloud_functions.dart';

import 'package:flutter_stripe_test/service/cloud_functions_service.dart';

class PaymentService {
  static Future handleCreditCardPayment(
      BillingDetails billingDetails, int amount, String currency) async {
    try {
      dynamic paymentIntentValues = {
        'currency': currency,
        'amount': amount,
      };

      final clientData = await _fetchPaymentIntentClient(paymentIntentValues);

      await Stripe.instance.confirmPayment(
        clientData['client_secret'],
        PaymentMethodParams.card(
          billingDetails: billingDetails,
          setupFutureUsage: null,
        ),
      );
      return PaymentResponseHandler('Success Payment', '', 200);
    } catch (e) {
      return PaymentResponseHandler('Error Payment', '', 500,
          error: e.toString());
    }
  }

  static Future<PaymentResponseHandler> handleCheckoutPayment(
      context, paymentData) async {
    try {
      final dynamic session = await _createCheckoutSession(paymentData);
      print('session id ${session['url']}');

      return PaymentResponseHandler(
          'Success Redirect to Checkout', session['url'], 200);
    } catch (e) {
      return PaymentResponseHandler('Error Payment', '', 500,
          error: e.toString());
    }
  }

  static Future<dynamic> _createCheckoutSession(paymentData) async {
    final HttpsCallableResult response = await _getCheckoutSession(paymentData);
    return response.data;
  }

  static Future _getCheckoutSession(payload) async {
    String functionName = 'stripeCreateCheckoutSession';
    return CloudFunctionsService.callFunction(functionName, payload);
  }

  static Future<Map<String, dynamic>> _fetchPaymentIntentClient(
      paymentIntentValues) async {
    final HttpsCallableResult response =
        await _getPaymentIntent(paymentIntentValues);
    return response.data;
  }

  static Future _getPaymentIntent(payload) async {
    String functionName = 'stripeCreatePaymentIntent';
    return CloudFunctionsService.callFunction(functionName, payload);
  }
}

class PaymentResponseHandler {
  String message;
  int code;
  String url;

  PaymentResponseHandler(this.message, this.url, this.code, {String? error});
}
